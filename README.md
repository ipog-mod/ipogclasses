[Symbol_Game]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Sprites/Other/IJWLC0
[Symbol_Important]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Graphics/IPOGFORW


# IPOG Classes

## ![Symbol_Game]**Description**
It's a classic Doom mod based on "In Pursuit of Greed" (IPOG), a not-so-known DOS game of the '90s. It has a ton of new features based on the game and many original ideas.

## ![Symbol_Game]**Compatibility**
Classic Doom source ports supporting ACS and DECORATE: GZDoom 4.2.4 or above (except 4.3 ones), LZDoom 3.84 or above (except 3.85). It might work on older versions.


## ![Symbol_Game]Features

- All IPOG characters added, each one with unique attributes, but keeping the classic Doom's gameplay!
- IPOG weapons with both primary and secondary fires!
- All IPOG inventory items are added and functional! Some were changed, and have unique power effects!
- Polished special effects!
- Custom skill levels!
- Custom help screens! Press F1 in-game to view them (or click on 'Read This!' in the main menu)!
- Enemies and Bonus Items may spawn in a random position of the map, like in IPOG (players can disable enemies spawning with a CVar)!
- Exit Nexus may also spawn in the map after picking up a Bonus Item and fulfilling two conditions: reach a score limit and no boss present on the map!
- Compatible with map packs (if they don't depend on class mods or change vanilla Doom too much)!
- Custom armor system! They are shield armor now (100% damage resistance).
- Custom scoring system!
- Custom weapon upgrade system! Each weapon upgrade lasts until the player dies!
- There's also a secret weapon in the mod!
- 10 Custom made powerups!
- Custom flags to personalize user gameplay as they wish!
- [Addon Pack] 36 enemies from IPOG, including 21 custom ones!


## ![Symbol_Game]Play Information

Games: Doom 1 (w/ IPOG Enemies Pack), Doom 2, Plutonia, Evilution, Heretic (w/ a Patch)

Gamemodes: Cooperative (Survival recommended, if available). Not compatible with Competitive modes (Death Match, Last Man Standing, CTF, and so on)

Custom Difficulty Settings: Yes

Recommended Renderer: OpenGL

Recommended HUD: Alternative


## ![Symbol_Game]File Order Usage Examples

| Example Description | File Order |
| ------ | ------ |
| Base mod only | IPOGClasses |
| Base mod with Addon Enemies Pack | IPOGClasses, IPOGEnemies |
| Base mod with Addon Enemies Pack and a Mappack | [mappack], IPOGClasses, IPOGEnemies |
| Base mod with Heretic | IPOGClasses, IPOGClasses_HereticPatch |
| Base mod with Addon Enemies Pack and Heretic | IPOGClasses, IPOGClasses_HereticPatch, IPOGEnemies |
| Base mod with Retribution Mode | IPOGClasses, IPOGClasses_Retribution |
| Base mod with Retribution Mode and a Mappack | [mappack], IPOGClasses, IPOGClasses_Retribution |
| Base mod with Addon Enemies Pack, Retribution Mode, and a Mappack | [mappack], IPOGClasses, IPOGEnemies, IPOGClasses_Retribution |
| Base mod with Addon Enemies Pack and Doom 1 Patch | IPOGClasses, IPOGEnemies, IPOGEnemies-Doom1Patch |


### ![Symbol_Important] **IMPORTANT:** Players should enable these CVars for better gameplay experience:

**compat_missileclip 1:** for projectiles to pass thru decorations

**hud_showscore 1:** to show score in the HUD

**am_showthingsprites 3:** to show things as sprites on Automap


## ![Symbol_Game]Custom CVars

#### ![Symbol_Important] NOTE: All CVars are dynamic, meaning they all take effect as soon as possible.

**IPOG_EnemyPortalEnabled <0|1\>:** Enables / Disables Enemy Portals, which spawn more enemies periodically.

**IPOG_RandomItemStart <0|1\>:** After joining the match, players begin with a single item chosen at random, which can be: Medical Tube, Shield Recharge, Grenade, Proximity Mine, Time Bomb, Decoy, InstaWall, or Eye of Enlightenment.

**IPOG_Greed <0|1\>:** When players pick up the Bonus Item, all enemies currently on the map will become fast for a short time.

**IPOG_JewelDrops <0|1\>:** Jewels give points and are spawned on the map and after killing enemies.

**IPOG_AmmoDrops <0|1\>:** Small ammo amounts are spawned on the map and after killing enemies.

**IPOG_WelfareDrops <0|1\>:** Small health or shield amounts are spawned on the map and after killing enemies.

**IPOG_NexusClearance <0|1\>:** When the Exit Nexus spawns, it prevents the spawning of Bonus Items and enemies from Enemy Portals.

**IPOG_ClearStart <0|1\>:** The player starts with pistol and melee after map changes.

**IPOG_DangerBonusItem <0|1\>:** After picking up the Bonus Item, the next enemy portal's wave spawns only better foes (non-bosses still).

**IPOG_ExitNexusEnabled <0|1\>:** Enables / Disables the Exit Nexus mechanic.

**IPOG_AutoUseWelfare <0|1\>:** Enables automatic usage of Health and Shield items when low on them.


## ![Symbol_Game]Skill Levels

### Brainless:
    - Ammo from map spawned ammo sources increased by 50%
    - Ammo from dropped ammo sources decreased by 15%
    - Ammo when double ammo is enabled increased by 100%
    - Monsters are less aggressive
    - Monsters are slower
    - Monsters will always infight
    - Maps have reduced monsters' amount by a lot
    - The map30-like boss is easier
    - Players get 50% less damage from all sources
    - Players get +25 HP and +25 AP boost for every map start (250 max for both)
    - Players begin with 50% extra ammo

### Average:
    - Ammo from map spawned ammo sources increased by 25%
    - Ammo from dropped ammo sources decreased by 30%
    - Ammo when double ammo is enabled increased by 100%
    - Monsters are a bit aggressive
    - Monsters are slow
    - Maps have reduced monsters' amount
    - The map30-like boss is easier
    - Players get 25% less damage from all sources
    - Players begin with 25% extra ammo

### Aggressive:
    - Ammo from map spawned ammo sources is normal
    - Ammo from dropped ammo sources decreased by 45%
    - Ammo when double ammo is enabled increased by 50%
    - Monsters are a bit more aggressive
    - Monsters' amount in maps is normal
    - Maps have normal monsters' amount
    - It's the default skill level

### Psychotic:
    - Ammo from map spawned ammo sources is normal
    - Ammo from dropped ammo sources decreased by 60%
    - Ammo when double ammo is enabled increased by 50%
    - Monsters are somewhat aggressive
    - Maps have increased monsters' amount

### Suicidal:
    - Ammo from map spawned ammo sources is normal
    - Ammo from dropped ammo sources decreased by 75%
    - Ammo when double ammo is enabled increased by 50%
    - Monsters are very aggressive
    - Maps have increased monsters' amount by a lot

### Demon Spawn:
    - Ammo from map spawned ammo sources is normal
    - Ammo from dropped ammo sources decreased by 90%
    - Double ammo has no effect
    - Monsters are absurdly aggressive
    - Maps have increased monsters' amount by a lot
    - Monsters will not infight
    - Disabled mutators by default: Random Item Start, Ammo Drops, Jewel Drops, Welfare Drops, and Weapon Drops
    - Enabled mutators by default: Greed and Danger Bonus Item


## ![Symbol_Game]Construction

Base: New mod from scratch

Technologies used: ACS, Audacity, Decorate, Geany, GIMP, Google Docs, Kate, Linux, Slade3, XnViewMP


## ![Symbol_Game]Credits

Mod by: Cyantusk

Sprites: In Pursuit of Greed (rips by Cyantusk and Snarboo - found in ZDoom forums)

Sprite Edit: Cyantusk

Sound and Music: In Pursuit of Greed

Sound Edit: Cyantusk

Decorate and ACS: Cyantusk

Opinions/suggestions to various details: C4rnage, CaptainJ, Criegrrunov, Dynamo, Grunky06, Waxey

Playtesting: Criegrrunov, Cyantusk, Grunky06

Special thanks: Id Software for Doom, Raven Software for Heretic and Hexen, Mind Shear Software for In Pursuit of Greed, Teams and contributors for ZDoom, GZDoom, LZDoom, and their variants


## ![Symbol_Game]Licensing

In Pursuit of Greed Classes project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

The data files (artwork, sound files, etc) are not covered in this license. The ones that came from In Pursuit of Greed have a [custom license](https://archive.org/details/GreedSource):

"These sources are provided for educational and historical purposes. No assets or code may be used in any way commercially. Personal and educational use only."

