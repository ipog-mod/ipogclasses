// In Pursuit of Greed Mod - Doom Modification for GZDoom
// Copyright (C) 2020-2025  William Weber Berrutti (aka Cyantusk)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

Actor "Heavy RocketLauncher" : IPOGWeapon replaces RocketLauncher 23003
{
    Weapon.Slotnumber 5
    Weapon.SelectionOrder 9
    Inventory.Pickupmessage "You got the Heavy Rocket Launcher!"
    Inventory.Pickupsound "*taunt"
    Weapon.AmmoType "RocketAmmo"
    Weapon.AmmoType2 "RocketAmmo"
    Weapon.AmmoUse 1
    Weapon.AmmoUse2 1
    Weapon.AmmoGive 10
    Scale 0.9
    +WEAPON.NOAUTOFIRE
    +Inventory.UNDROPPABLE
    +DONTGIB
    States
    {
    Spawn:
        RKTL Z -1 Bright
        Stop
        
    Ready:
        TNT1 A 0 A_JumpIfInventory("IPOGUpgradeWeap5", 1, "ReadyPowered")
        RKTL A 1 A_WeaponReady
        Loop

    ReadyPowered:
        RKTL DEFGHGFE 4 A_WeaponReady
        Loop

    Select:
        TNT1 AAA 0 A_Raise
        RKTL A 1 A_Raise
        Loop

    Deselect:
        TNT1 AAA 0 A_Lower
        RKTL A 1 A_Lower
        Loop
    
    Fire:
        TNT1 A 0 A_JumpIfInventory("DimensionalHolePower", 1, "Ready")
        
        TNT1 A 0 A_JumpIfInventory("IPOGUpgradeWeap5", 1, "FirePowered")
        RKTL B 0 Bright A_PlayWeaponSound("greed/rocket")
        RKTL B 3 Bright A_FireProjectile("HeavyRocket",0,1,0,4)
        RKTL C 4
        RKTL B 4
        RKTL A 11
        TNT1 A 0 A_Refire
        Goto Ready
        
    FirePowered:
        RKTL B 0 Bright A_PlayWeaponSound("greed/rocket")
        RKTL B 3 Bright A_FireProjectile("HeavyRocket_Powered",0,1,0,4)
        RKTL C 4
        RKTL B 4
        RKTL HGFED 2
        RKTL D 1
        TNT1 A 0 A_Refire
        Goto Ready
        
    AltFire:
        TNT1 A 0 A_JumpIfInventory("DimensionalHolePower", 1, "Ready")
        
        TNT1 A 0 A_JumpIfInventory("IPOGUpgradeWeap5", 1, "AltFirePowered")
        RKTL B 0 Bright A_PlayWeaponSound("greed/rocket")
        RKTL B 3 Bright A_FireProjectile("HeavyRocket_Seeker",0,1,0,4)
        RKTL C 4
        RKTL B 4
        RKTL A 15
        TNT1 A 0 A_Refire
        Goto Ready
        
    AltFirePowered:
        RKTL B 0 Bright A_PlayWeaponSound("greed/rocket")
        RKTL B 3 Bright A_FireProjectile("HeavyRocket_Seeker_Powered",0,1,0,4)
        RKTL C 4
        RKTL B 4
        RKTL EFGHGFE 2
        RKTL D 1
        TNT1 A 0 A_Refire
        Goto Ready
    }
}

Actor HeavyRocket : FastProjectile
{
    Radius 8
    Height 8
    Speed 35
    PROJECTILE
    +BLOODLESSIMPACT
    +FORCEXYBILLBOARD
    +THRUGHOST
    Damage (100)
    DamageType "HeavyRocketExplosive"
    Scale 0.375
    Species "Player"
    +DONTHURTSPECIES
    +DONTHARMSPECIES
    +THRUSPECIES
    States
    {
    Spawn:
        RLPJ A 1 Bright Light("RocketProjectile") A_SpawnItemEx("IPOGBulletPuffRocket")
        Loop
    Death:
        TNT1 A 0 A_Explode(100,136,1,0,16)
        TNT1 A 0 A_SpawnItemEx("RocketEffect")
        Stop
    }
}

Actor HeavyRocket_Powered : HeavyRocket
{
    Speed 40
    Damage (100)
    -THRUGHOST
    States
    {
    Spawn:
        RLPJ B 1 Bright Light("RocketProjectile_Powered") A_SpawnItemEx("IPOGBulletPuffRocket_Seeker")
        Loop
    Death:
        TNT1 A 0 A_Explode(100,136,1,0,16)
        TNT1 A 0 A_ChangeFlag(FORCERADIUSDMG, 1)
        TNT1 A 0 A_Explode(50,136,1,0,16)
        TNT1 A 0 A_SpawnItemEx("RocketEffect_Blue")
        Stop
    }
}

Actor HeavyRocket_Seeker : HeavyRocket
{
    Speed 25
    +SEEKERMISSILE
    +SCREENSEEKER
    +DONTSEEKINVISIBLE
    ReactionTime 256
    States
    {
    Spawn:
        RLPJ A 1 NoDelay Bright Light("RocketProjectile")
        RLPJ A 0 A_SpawnItemEx("IPOGBulletPuffRocket")
        RLPJ A 1 Bright Light("RocketProjectile")
        RLPJ A 0 A_SpawnItemEx("IPOGBulletPuffRocket")
        Goto StraightLine
    StraightLine:
        RLPJ A 1 Bright Light("RocketProjectile") A_SeekerMissile(5,5,SMF_LOOK|SMF_PRECISE,256)
        RLPJ A 0 A_SpawnItemEx("IPOGBulletPuffRocket")
        Loop
    Death:
        TNT1 A 0 A_Explode(100,136,1,0,16)
        TNT1 A 0 A_SpawnItemEx("RocketEffect")
        Stop
    }
}

Actor HeavyRocket_Seeker_Powered : HeavyRocket_Seeker
{
    Speed 30
    -DONTSEEKINVISIBLE
    -THRUGHOST
    States
    {
    Spawn:
        RLPJ B 1 NoDelay Bright Light("RocketProjectile_Powered")
        RLPJ A 0 A_SpawnItemEx("IPOGBulletPuffRocket_Seeker")
        RLPJ B 1 Bright Light("RocketProjectile_Powered")
        RLPJ A 0 A_SpawnItemEx("IPOGBulletPuffRocket_Seeker")
        Goto StraightLine
    StraightLine:
        RLPJ B 1 Bright Light("RocketProjectile_Powered") A_SeekerMissile(8,8,SMF_LOOK|SMF_PRECISE,256)
        RLPJ A 0 A_SpawnItemEx("IPOGBulletPuffRocket_Seeker")
        Loop
    Death:
        TNT1 A 0 A_Explode(100,136,1,0,16)
        TNT1 A 0 A_ChangeFlag(FORCERADIUSDMG, 1)
        TNT1 A 0 A_Explode(50,136,1,0,16)
        TNT1 A 0 A_SpawnItemEx("RocketEffect_Blue")
        Stop
    }
}

Actor IPOGBulletPuffRocket : IPOGBulletPuff
{
    +FORCEXYBILLBOARD
    Alpha 0.3
    Scale 0.35
    States
    {
    Spawn:
    Crash:
        TNT1 A 2
        BPUF ABCD 3 Bright
        Stop
    Melee:
        TNT1 A 1
        Stop
    }
}

Actor IPOGBulletPuffRocket_Seeker : IPOGBulletPuffRocket
{
    States
    {
    Spawn:
    Crash:
        TNT1 A 2
        BPUF EFGH 3 Bright
        Stop
    Melee:
        TNT1 A 1
        Stop
    }
}

Actor RocketPuff : IPOGProjectileEffectBase
{
    +THRUACTORS
    RenderStyle Translucent
    Alpha 0.75
    Scale 0.8
    BounceType "Hexen"
    BounceCount 3
    BounceFactor 1
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_PlaySound("greed/explosion", CHAN_AUTO)
        EXP1 ABCD 4 Bright Light("RocketPuff")
        Stop
    }
}

Actor RocketMediumPuff : RocketPuff
{
    Scale 0.5375
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_PlaySound("greed/explosion", CHAN_AUTO)
        EXP1 ABCD 4 Bright Light("RocketMediumPuff")
        Stop
    }
}

Actor RocketMediumPuff_Blue : RocketPuff
{
    Scale 0.5375
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_PlaySound("greed/explosion", CHAN_AUTO)
        EXP3 ABCD 4 Bright Light("RocketMediumPuff_Blue")
        Stop
    }
}

Actor RocketSmallPuff : RocketPuff
{
    Scale 0.65
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP1 ABCD 4 Bright Light("RocketSmallPuff")
        Stop
    Death02:
        EXP2 ABCD 4 Bright Light("RocketSmallPuff")
        Stop
    }
}

Actor RocketPuff_Blue : IPOGProjectileEffectBase
{
    +THRUACTORS
    RenderStyle Translucent
    Alpha 0.75
    Scale 0.8
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_PlaySound("greed/explosion", CHAN_AUTO)
        EXP3 ABCD 4 Bright Light("RocketPuff_Blue")
        Stop
    }
}

Actor RocketSmallPuff_Blue : RocketPuff_Blue
{
    Scale 0.65
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP3 ABCD 4 Bright Light("RocketSmallPuff_Blue")
        Stop
    Death02:
        EXP4 ABCD 4 Bright Light("RocketSmallPuff_Blue")
        Stop
    }
}

Actor RocketTinyPuff : RocketSmallPuff
{
    Scale 0.425
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP1 ABCD 4 Bright Light("RocketTinyPuff")
        Stop
    Death02:
        EXP2 ABCD 4 Bright Light("RocketTinyPuff")
        Stop
    }
}

Actor RocketTinyPuff_Blue : RocketSmallPuff_Blue
{
    Scale 0.425
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP3 ABCD 4 Bright Light("RocketTinyPuff_Blue")
        Stop
    Death02:
        EXP4 ABCD 4 Bright Light("RocketTinyPuff_Blue")
        Stop
    }
}

Actor RocketPuff_Red : RocketPuff
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_PlaySound("greed/explosion", CHAN_AUTO)
        EXP5 ABCD 3 Bright Light("RocketPuff_Red")
        Stop
    }
}

Actor RocketSmallPuff_Red : RocketPuff_Red
{
    Scale 0.65
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP5 ABCD 3 Bright Light("RocketSmallPuff_Red")
        Stop
    Death02:
        EXP6 ABCD 3 Bright Light("RocketSmallPuff_Red")
        Stop
    }
}

Actor RocketVeryTinyPuff : RocketSmallPuff
{
    Scale 0.275

    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP1 ABCD 4 Bright Light("RocketVeryTinyPuff")
        Stop
    Death02:
        EXP2 ABCD 4 Bright Light("RocketVeryTinyPuff")
        Stop
    }
}

Actor RocketVeryTinyPuff_Fade : RocketSmallPuff
{
    Alpha 1.0
    Scale 0.175
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_Jump(256, "Death01", "Death02")
        Stop
    Death01:
        EXP1 AAAABBBBCCCCDDDD 1 Bright Light("RocketVeryTinyPuff") A_FadeOut(0.075)
        Stop
    Death02:
        EXP2 AAAABBBBCCCCDDDD 1 Bright Light("RocketVeryTinyPuff") A_FadeOut(0.075)
        Stop
    }
}

Actor RocketEffect_Base { +CLIENTSIDEONLY +NOGRAVITY }

Actor RocketEffect : RocketEffect_Base 15880
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketPuff")
        TNT1 AAAAAAAA 1 A_SpawnItemEx("RocketSmallPuff",0,0,0,FRandom(-2,2),FRandom(-2,2),FRandom(-2,2),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketEffect_Blue : RocketEffect_Base 15155
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketPuff_Blue")
        TNT1 AAAAAAAA 1 A_SpawnItemEx("RocketSmallPuff_Blue",0,0,0,FRandom(-2,2),FRandom(-2,2),FRandom(-2,2),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketEffect_Small_Blue : RocketEffect_Base 15156
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketMediumPuff_Blue")
        TNT1 AAAAA 1 A_SpawnItemEx("RocketTinyPuff_Blue",0,0,0,FRandom(-1,1),FRandom(-1,1),FRandom(-1,1),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketEffect_Medium : RocketEffect_Base 15881
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketPuff")
        TNT1 AAAAAAAA 1 A_SpawnItemEx("RocketMediumPuff",0,0,0,FRandom(-1.5,1.5),FRandom(-1.5,1.5),FRandom(-1.5,1.5),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketEffect_Small : RocketEffect_Base 15882
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketMediumPuff")
        TNT1 AAAAA 1 A_SpawnItemEx("RocketTinyPuff",0,0,0,FRandom(-1,1),FRandom(-1,1),FRandom(-1,1),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketEffect_Tiny : RocketEffect_Base 15883
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketTinyPuff")
        TNT1 AAAAA 1 A_SpawnItemEx("RocketVeryTinyPuff",0,0,0,FRandom(-0.75,0.75),FRandom(-0.75,0.75),FRandom(-0.75,0.75),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketEffect_Red : RocketEffect_Base 15884
{
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("RocketPuff_Red")
        TNT1 AAAAAAAA 1 A_SpawnItemEx("RocketSmallPuff_Red",0,0,0,FRandom(-2,2),FRandom(-2,2),FRandom(-2,2),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_CLIENTSIDE)
        Stop
    }
}

Actor RocketPuff2 : RocketPuff { -MISSILE -SOLID }

Actor RocketSmallPuff2 : RocketSmallPuff { -MISSILE -SOLID }
