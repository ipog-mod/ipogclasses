// In Pursuit of Greed Mod - Doom Modification for GZDoom
// Copyright (C) 2020-2025  William Weber Berrutti (aka Cyantusk)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

Actor BonusItemBase : AuxiliaryCustomInventory
{
    Radius 16
    Height 28
    Inventory.PickupSound "*taunt"
    +COUNTITEM
    +DONTGIB
    +MOVEWITHSECTOR
    +NOTIMEFREEZE
    //$Category "IPOG Bonus Item"
    States
    {
    Use:
        TNT1 A 0 ACS_NamedExecuteAlways("ArmorToShieldConverter")
        TNT1 A 0 A_GiveInventory("HealthBonusItem", 1)
        TNT1 A 0 A_GiveInventory("ArmorBonusItem", 1)
        TNT1 A 0 A_GiveInventory("ScoreItem", 2500)
        Stop
    }
}

Actor BonusItem : BonusItemBase 15000
{
    Inventory.PickupMessage "You got a Bonus Item!"
    Gravity 0
    States
    {
    Spawn:
        TNT1 A 0 NoDelay A_SpawnItemEx("IPOGBonusItemFog")
        TNT1 A 0 A_PrintBold("\cnBonus Item spawned!\n\cnPlayers have 1 minute to steal it!", 3.0)
        TNT1 A 0 A_SpawnItemEx("BonusItemParticleSpawner",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS)
        TNT1 A 0 A_Jump(256, "Item1", "Item2", "Item3", "Item4", "Item5", "Item6", "Item7", "Item8", "Item9", "Item10",
                             "Item11", "Item12", "Item13", "Item14", "Item15", "Item16", "Item17", "Item18", "Item19", "Item20",
                             "Item21", "Item22")
        Stop
    Item1:
        ITEM A 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item2:
        ITEM B 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item3:
        ITEM C 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item4:
        ITEM D 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item5:
        ITEM E 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item6:
        ITEM F 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item7:
        ITEM G 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item8:
        ITEM H 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item9:
        ITEM I 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item10:
        ITEM J 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item11:
        ITEM K 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item12:
        ITEM L 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item13:
        ITEM M 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item14:
        ITEM N 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item15:
        ITEM O 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item16:
        ITEM P 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item17:
        ITEM Q 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item18:
        ITEM R 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item19:
        ITEM S 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item20:
        ITEM T 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item21:
        ITEM U 2100 Bright Light("BonusItemLight")
        Goto Disappear
    Item22:
        ITEM V 2100 Bright Light("BonusItemLight")
        Goto Disappear
        
    Pickup:
        TNT1 A 0 ACS_NamedExecute("BonusItem_GlobalPlayerMessage")
        TNT1 A 0 A_RadiusGive("BonusItemTaken", 16383, RGF_GIVESELF|RGF_PLAYERS|RGF_CUBE|RGF_NOSIGHT, 1)
        TNT1 A 0 A_RadiusGive("BonusItem_GreedMutator", 16383, RGF_MONSTERS|RGF_CUBE|RGF_NOSIGHT, 1)
        // I wanted to do a generic skill level check, but I don't know how to call an ACS function in Decorate at the moment
        TNT1 A 0 A_JumpIf(GetCVar("ipog_dangerbonusitem") || ACS_NamedExecuteWithResult("CheckSkill") == 5, "GiveDangerItem")
        Stop
        
    GiveDangerItem:
        TNT1 A 0 A_RadiusGive("IPOGDangerBonusItem", 16383, RGF_MONSTERS|RGF_CUBE|RGF_NOSIGHT, 1)
        Stop
        
    Disappear:
        TNT1 A 0 A_PrintBold("\cnThe bonus item disappeared! No one stole it!", 3.0)
        TNT1 A 0 A_SpawnItemEx("IPOGBonusItemFog")
        TNT1 A 0 A_PlaySound("misc/teleport")
        Stop
    }
}
